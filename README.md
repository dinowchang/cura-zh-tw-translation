Cura 正體中文翻譯
=================

下載方式
--------

到專案的網頁 [Cura-zh-TW-translation](https://gitlab.com/dinowchang/cura-zh-tw-translation) 點擊下載按鈕



軟體安裝
--------

請到 https://poedit.net/ 下載最新 POEDIT 安裝即可

# 翻譯對照
記錄需要統一的翻譯名詞和句型


## 名詞翻譯對照表

英文 | 中文 | 備註
-----|-----|-----
abort | 中斷
access | 存取
adhesion | 附著
apply | 套用
block | 暫停 | 參考 windows 在印表機工作的翻譯
brim | 邊緣
buildplate | 列印平台
build volume | 列印範圍 / 列印空間 | 字面意思難以翻譯，以意義翻譯
calibration | 校正
close | 關閉
coarse | 粗糙
cross | 十字
custom | 自訂
details | 細項設定
eject | 卸載
export | 匯出
extruder | 擠出機
factor | 係數
filament | 耗材
fine | 精細
fuzzy | 絨毛
gantry | 吊車
generate | 產生
image | 圖片
import | 匯入
information | 資訊
ironing | 燙平
jerk | 加加速度
jitter | 抖動
job | 作業
license | 授權協議
list | 清單
load | 載入
material | 耗材
mesh | 網格
metadata | metadata | 暫查無適合翻譯
model | 模型
motor | 馬達
nozzle | 噴頭
object | 物件
ooze | 滲出
open | 開啟
outline | 輪廓
overhang | 突出部分
override | 覆寫 / 覆蓋
package | 套件
pattern | 樣式
pause | 暫停
pocket (cross 3D) | 氣囊
preferences | 偏好設定
prime tower | 換料塔
print core | print core | 暫查無適合翻譯
print job | 列印作業
printer | 印表機
profile | 列印參數 | 為了與 configuration 和 setting 等區別，翻做"參數"。<br>為了與耗材和機器的參數區別，再加上"列印"。
project | 專案
queue | 隊列
raft | 木筏 | 原文是"木筏"，不知為何有人翻成"棧板"(pallet)。<br>而且棧板並不是大家熟悉的物品，因此依照原文翻譯。
raft top | 木筏頂部
raft middle | 木筏中層
raft base | 木筏底部
reader | 讀取器
redo | 取消復原 | 參考 evernote 的翻譯
removable drive | 行動裝置
reset | 重置
resume | 繼續
save | 儲存
skirt | 外圍 | 翻成襯裙很奇怪，中文也沒有使用裙子表示週圍的用法。<br>因此選擇翻成 skirt 的另一個意義。
slice | 切片
small feature | 細部模式
step | 步階
travel | 空跑
undo | 復原 | 參考 evernote 的翻譯
upgrade | 升級
view | 檢視
visibility | 顯示設定
wire printing | 鐵絲網列印
writer | 寫入器

## 句型翻譯對照表
- Provides support ...
  提供 ... 的支援